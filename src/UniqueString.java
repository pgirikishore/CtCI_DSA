
/**
 * @author Giri
 *
 */
public class UniqueString {
	static boolean isUnique(String s)
	{
		boolean[] chars = new boolean[128];
		for (int i=0;i<s.length();i++)
		{
			int ch = s.charAt(i);
			if (chars[ch] == true)
			{
				return false;
			}
			chars[ch] = true;
		}
		return true;
	}
	public static void main (String[] args) {
		System.out.println(isUnique("New"));
		System.out.println(isUnique("Horizon"));
		System.out.println(isUnique("College"));
		System.out.println(isUnique("of"));
		System.out.println(isUnique("Engineering"));
	}
}
