package Library;

public class LinkedListNode {
	public LinkedListNode next;
	public LinkedListNode prev;
	public int data;
	
	public LinkedListNode(int data) {
		this.next = null;
		this.prev = null;
		this.data = data;
	}
	
	public LinkedListNode() {}
	
	public String printList()
	{
		if (next != null) {
			return data + "->" + next.printList();
		} else {
			return ((Integer) data).toString();
		}
	}
	
	public LinkedListNode addToHead(LinkedListNode node)
	{
		if (node != null)
		node.next = this;
		return node;
	}
	
}
