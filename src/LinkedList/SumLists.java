package LinkedList;

import Library.LinkedListNode;

public class SumLists {
	static LinkedListNode res = null;
//	static int carry = 0;
	public static LinkedListNode sumOfLists(LinkedListNode h1, LinkedListNode h2, int carry)
	{
		LinkedListNode h3 = null;
		if (h1 == null && h2 == null && carry == 0)
		{
			return null;
		}
		int val = carry;
		
		if (h1 != null)
			val += h1.data;
		if (h2!=null)
			val += h2.data;
		if (val >= 10)
			carry = 1;
		else
			carry = 0;

		h3 = new LinkedListNode(val%10);
		if (h1 != null || h2 != null)
			sumOfLists(h1!=null?h1.next:null, h2!=null?h2.next:null, carry);
		
		if (res != null)
			res = res.addToHead(h3);
		else
			res = h3;
		return res;
	}

//	public static LinkedListNode sumOfLists(LinkedListNode h1, LinkedListNode h2)
//	{
//		LinkedListNode h3 = null;
//		if (h1 == null && h2 == null && carry == 0)
//		{
//			return null;
//		}
//		sumOfLists(h1!=null?h1.next:null, h2!=null?h2.next:null);
//		
//		int val = carry;
//		
//		if (h1 != null)
//			val += h1.data;
//		if (h2!=null)
//			val += h2.data;
//		if (val >= 10)
//			carry = 1;
//		else
//			carry = 0;
//
//		h3 = new LinkedListNode(val%10);
//		
//		if (res != null)
//			res = res.addToHead(h3);
//		else
//			res = h3;
//	    return res;
//	}
	public static void main(String[] args) {
		int[] arr = {1,1,7};
		LinkedListNode head1, node1, node2;
		head1 = new LinkedListNode(arr[0]);
		node1 = new LinkedListNode();
		node1 = head1;
		for(int i=1;i<arr.length;i++)
		{
			node2 = new LinkedListNode(arr[i]);
			node2.next = null;
			node1.next = node2;
			node1 = node2;
		}
		
		int[] arr2 = {1,9,4};
		LinkedListNode head2;
		head2 = new LinkedListNode(arr2[0]);
		node1 = new LinkedListNode();
		node1 = head2;
		for(int i=1;i<arr2.length;i++)
		{
			node2 = new LinkedListNode(arr2[i]);
			node2.next = null;
			node1.next = node2;
			node1 = node2;
		}
		
		System.out.println(sumOfLists(head1, head2, 0).printList());


	}

}
