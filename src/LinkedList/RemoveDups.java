package LinkedList;

import java.util.HashSet;
import Library.LinkedListNode;

public class RemoveDups {
	
	public static void removeDuplicateWithoutBuffer(LinkedListNode head)
	{
		LinkedListNode node1 = head;
		LinkedListNode node2 = null;
		while(node1 != null)
		{
			node2 = node1.next;
			while(node2 != null)
			{
				if(node1.data == node2.data)
				{
					node2.prev.next = node2.next;
					if(node2.next != null)
					{
					    node2.next.prev = node2.prev;
					}
				}
				node2 = node2.next;
			}
			node1 = node1.next;
		}
	}

	public static void removeDuplicateWithBuffer(LinkedListNode head)
	{
		HashSet<Integer> hs = new HashSet<>();
		LinkedListNode node = head;
		while(node != null)
		{
			if (hs.contains(node.data))
			{
				node.prev.next = node.next;
				if (node.next != null)
				{
					node.next.prev = node.prev;
				}
			}
			else
			{
				hs.add(node.data);
			}
			node = node.next;
		}
	}
	public static void main(String[] args) {
		int[] arr = {1,1,3,1,5,6,7,8,10,10};
		LinkedListNode head, node1, node2;
		head = new LinkedListNode(arr[0]);
		node1 = new LinkedListNode();
		node1 = head;
		for(int i=1;i<arr.length;i++)
		{
			node2 = new LinkedListNode(arr[i]);
			node2.next = null;
			node2.prev = node1;
			node1.next = node2;
			node1 = node2;
		}
		System.out.println("Linked List before removal");
		System.out.println(head.printList());
		
		removeDuplicateWithBuffer(head);
		
		System.out.println("Linked List after removal with buffer");
		System.out.println(head.printList());
		
        removeDuplicateWithoutBuffer(head);
		
		System.out.println("Linked List after removal without buffer");
		System.out.println(head.printList());
	}

}
