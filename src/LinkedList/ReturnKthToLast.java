package LinkedList;

import Library.LinkedListNode;

public class ReturnKthToLast {
	static int k = 0;
	
	public static void returnFunc(LinkedListNode head)
	{
		if (head == null)
		{
			return;
		}
		returnFunc(head.next);
		k++;
		if (k == 5)
		{
			System.out.println(head.data);
			return;
		}
		return;
	}

	public static void main(String[] args) {
		int[] arr = {1,1,3,1,5,6,7,8,10,10};
		LinkedListNode head, node1, node2;
		head = new LinkedListNode(arr[0]);
		node1 = new LinkedListNode();
		node1 = head;
		for(int i=1;i<arr.length;i++)
		{
			node2 = new LinkedListNode(arr[i]);
			node2.next = null;
			node1.next = node2;
			node1 = node2;
		}
		System.out.println("Linked List");
		System.out.println(head.printList());
		
		returnFunc(head);

	}

}
